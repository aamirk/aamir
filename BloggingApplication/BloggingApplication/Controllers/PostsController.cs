﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BloggingApplication.Models;

namespace BloggingApplication.Controllers
{
    public class PostsController : Controller
    {
        // Read access to model
        private BloggingModel model = new BloggingModel();
        private const int PostsPerPage = 4;
        //
        // GET: /Posts/

        public ActionResult Index(int? id)
        {
            int pageNumber = id ?? 0;
            IEnumerable<post> posts =
                (from post in model.posts
                where post.datetime < DateTime.Now
                orderby post.datetime descending
                select post).Skip(pageNumber * PostsPerPage).Take(PostsPerPage + 1);
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = posts.Count() > PostsPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.IsAdmin = isAdmin;
            return View(posts.Take(PostsPerPage));
        }

        [ValidateInput(false)]
        public ActionResult Update(int? id, string title, string body, DateTime datetime, string tags)
        {
            if (!isAdmin)
            {
                return RedirectToAction("Index");
            }

            post post = GetPost(id);
            post.title = title;
            post.body = body;
            post.datetime = datetime;
            post.tags.Clear();

            tags = tags ?? string.Empty;
            string[] tagNames = tags.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string tagName in tagNames)
            {
                post.tags.Add(GetTag(tagName));
            }

            if (!id.HasValue)
            {
                model.posts.Add(post);
            }
            model.SaveChanges();
            return RedirectToAction("Details", new { id = post.id });
        }

        private tag GetTag(string tagName)
        {
            return model.tags.Where(x => x.name == tagName).FirstOrDefault() ?? new tag { name = tagName };
        }

        private tag GetTags(int? tagId)
        {
            return model.tags.Where(x => x.id == tagId.Value).FirstOrDefault() ?? new tag { id = tagId.Value };
        }

        private post GetPost(int? id)
        {
            return id.HasValue ? model.posts.Where(x => x.id == id).First() : new post() { id = -1 };
        }

        // TODO: don't just return true
        public bool isAdmin { get { return true; } }

        public ActionResult Edit(int? id)
        {
            post post = GetPost(id);
            StringBuilder tagList = new StringBuilder();
            foreach (tag tag in post.tags)
            {
                tagList.AppendFormat("{0}", tag.name);
            }
            ViewBag.tags = tagList.ToString();
            return View(post);
        }

        public ActionResult Detail(int id)
        {
            post post = GetPost(id);
            ViewBag.IsAdmin = isAdmin;
            return View(post);
        }

        [ValidateInput(false)]
        public ActionResult Comment(int id, string commentername, string email, string body)
        {
            post post = GetPost(id);
            comment comment = new comment();
            comment.post = post;
            comment.datetime = DateTime.Now;
            comment.commentername = commentername;
            comment.email = email;
            comment.body = body;
            model.comments.Add(comment);
            model.SaveChanges();
            return RedirectToAction("Detail", new { id = id });
        }
        
        public ActionResult Tags(int? tagId)
        {
            tag tag = GetTags(tagId);
            ViewBag.IsAdmin = isAdmin;
            return View("Index", tag.posts);
        }
    }
}
